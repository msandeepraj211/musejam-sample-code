var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/*var db = mongoose.connect('mongodb://msandeepraj211:manchala211@ds039000.mongolab.com:39000/ani',function (err) {
    if (err) {
        console.error('\x1b[31m', 'Could not connect to MongoDB!');
        console.log(err);
    }else{console.log("connected")}
});
*/

var aniSchema = new Schema({
    "videos" : [
        {
            "pageTitle" : {type:String, default:"Sorry pageTitle Not Avalible"},
            "pageCatogory" : {type:String, default:"Sorry pageCatogory Not Avalible"},
            "pageCatogoryLink" : {type:String, default:"Sorry pageCatogoryLink Not Avalible"},
            "links" : []
        }
    ],
    "title" : {type:String, default:"Sorry Title Not Avalible"},
    "description" :{type:String, default:"Sorry Description Not Avalible"},
    "image" : {type:String, default:"DeathByAnime.jpg"},
    "genre" : []
});

mongoose.model('ani', aniSchema,'anime');