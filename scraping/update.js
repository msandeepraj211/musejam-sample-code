var CronJob = require('cron').CronJob;
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var mongoose = require('mongoose');
var ani = mongoose.model('ani');
// twitter oauth and settings
var util = require('util'),
    twitter = require('twitter');
var twit = new twitter({
    consumer_key: '',
    consumer_secret: '',
    access_token_key: '',
    access_token_secret: ''
});
//end of twitter settings
new CronJob('0 0 * * * *', function () {
    console.log('You will see this message every hour');
    url = 'http://www.gogoanime.com';
    request(url, function (error, response, html) {  //open website gogoanime.com
        if (error) {
            console.log('error:' + error);
        }
        if (!error) {
            var $ = cheerio.load(html);
            var content = fs.readFileSync('animes/recent.json');
            var recent = JSON.parse(content); //get previously added recent episodes
            var k = 25;
            //get number of episodes added after last update.
            if (recent.length > 0) {
                for (var i = 25; i > 0; i = i - 1) {
                    if ($('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li font').html() == '(Sub)' || $('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li font').html() == '(Dub)') {
                        if (recent[recent.length - 1].name == $('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li a').html()) {
                            k = i - 1;
                        }
                    }
                }
            }
            //define a function to scrape new episodes and add to database
            addEpisode = function (i) {
                console.log('function called' + i);
                if (i == 0) {
                    fs.writeFileSync('animes/recent.json', JSON.stringify(recent));
                }
                if (i > 0) {
                    if ($('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li font').html() == '(Sub)' || $('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li font').html() == '(Dub)') {
                        var tempdata = {};
                        var tempdata1 = {};
                        tempdata.name = $('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li a').html();
                        tempdata.link = $('#content .post div:nth-child(2) table tr td:nth-child(1) ul:nth-child(' + i + ') li a').attr('href');
                        recent.push(tempdata);
                        console.log(tempdata);
                        //open the new episode link
                        request(tempdata.link, function (error, response, html) {
                            if (error) {
                                console.log('error: ' + error);
                                addEpisode(i);
                            }
                            if (!error) {
                                console.log('opened episodeList? ' + tempdata.link + " : ");
                                var $ = cheerio.load(html);
                                //get details from the page scource code.
                                tempdata1.pageTitle = $('.post .postdesc h1').html();
                                tempdata1.pageCatogory = $('.post .postdesc:nth-child(1) p a').html();
                                tempdata1.pageCatogoryLink = $('.post .postdesc:nth-child(1) p a').attr('href');
                                tempdata1.links = [];
                                for (var j = 1; j <= $('.postcontent p').length; j = j + 1) {
                                    tempdata1.links.push($('.postcontent p:nth-child(' + j + ') iframe').attr('src'));
                                }
                            }
                            //check if anime episode already present in the database
                            ani.findOne({"videos.pageTitle": tempdata1.pageTitle}).exec(function (err, doc) {
                                if (err) {
                                    console.log(err);
                                    res.send('error occured while searching for the key word')
                                }

                                if (doc == null) {
                                    // query and open the concerned anime in database
                                    ani.findOne({title: tempdata1.pageCatogory}).exec(function (err, docs) {
                                        if (err) {
                                            console.log(err);
                                            res.send('error occured while searching for the key word')
                                        }
                                        ;
                                        result = docs;
                                        //check if the anime already present or add a new one
                                        if (result != null) {
                                            //if anime already present add the new episode to the list and save.
                                            result.videos.unshift(tempdata1);
                                            result.save();
                                            //twitter tweet that we added new epidode


                                            var status = 'check out new episode of ' + result.title + ' at (www.deathbyanime.com/episoide/' + encodeURIComponent(tempdata1.pageTitle) + ')';

                                            twit.updateStatus(status, function (data) {
                                                console.log('successfully tweeted');
                                            });

                                            //tweet end

                                            //call the function again for next updated anime
                                            addEpisode(i - 1);
                                            //check if the newly added episode is an movie but not regular series
                                        } else if(tempdata1.pageCatogory=="Anime Movies"){
                                            //if it is a anime movie move on to next episode
                                            addEpisode(i-1);

                                        }else {
                                            //if the anime is not present already call a function to add the whole episodes to database
                                            addNew(tempdata1);
                                            addEpisode(i - 1);
                                        }
                                    });
                                } else {
                                    console.log('episode already exist');
                                    addEpisode(i - 1);
                                }


                            });
                        });
                    } else {
                        addEpisode(i - 1);
                    }
                }

            }
//call the function to add episodes.
            addEpisode(k);
        }
    });
    download = function (uri, filename, callback) {
        request.head(uri, function (err, res, body) {
            console.log('content-type:', res.headers['content-type']);
            console.log('content-length:', res.headers['content-length']);

            request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
        });
    };
    //scrape pages
    pages = function (m, link, data) {
        console.log('pages function entered');
        nextPresent = false;
        if (m == 1) {
            console.log('define episodeList');
            episodeList = [];
        }
        url2 = link + '/page/' + m
        request(url2, function (error, response, html) {
            nextPresent = false;
            if (error) {
                console.log('error: ' + error);
                pages(m);
            }
            if (!error) {
                var $ = cheerio.load(html);
                if (m == 1) {
                    var tempdata = {};
                    tempdata.title = $('.pagetitle').html();
                    tempdata.description = $('.catdescription').html();
                    episodeList.push(tempdata);
                }
                for (var i = 1; i <= $('#content .postlist').length; i = i + 1) {
                    var j = i + 8;
                    var tempdata = {};
                    tempdata.link = $('#content .postlist:nth-child(' + j + ') table tr td a').attr('href');
                    tempdata.name = $('#content .postlist:nth-child(' + j + ') table tr td a').html();
                    JSON.stringify(tempdata);
                    episodeList.push(tempdata);
                }
                for (var n = 0; n < $('.wp-pagenavi a').length; n = n + 1) {
                    var j = n + 2;
                    if ($('.wp-pagenavi a:nth-child(' + j + ')').html() == 'Next') {
                        nextPresent = true;
                    }
                }
                if (!nextPresent) {
                    videos(episodeList, data);
                    console.log('videos called');
                }
                if (nextPresent) {
                    pages(m + 1, link, data);
                    console.log('next page called');
                }
            }
        })
    }
    //scrape vvideo links
    videos = function (epiList, data) {
        console.log('videos entered');

        var content1 = fs.readFileSync('animes/new.json');
        var newSeries = JSON.parse(content1);
        console.log(epiList);
        function extract(i) {
            console.log('extract entered');
            var tempdata1 = {};
            console.log(epiList[i]);
            request(epiList[i].link, function (error, response, html) {
                if (error) {
                    console.log('error: ' + error);
                    extract(i);
                }
                if (!error) {
                    console.log('opened episodeList? ' + epiList[i].link + " : ");
                    var $ = cheerio.load(html);
                    tempdata1.pageTitle = $('.post .postdesc h1').html();
                    tempdata1.pageCatogory = $('.post .postdesc:nth-child(1) p a').html();
                    tempdata1.pageCatogoryLink = $('.post .postdesc:nth-child(1) p a').attr('href');
                    tempdata1.links = [];
                    for (var j = 1; j <= $('.postcontent p').length; j = j + 1) {
                        tempdata1.links.push($('.postcontent p:nth-child(' + j + ') iframe').attr('src'));
                    }
                    data.videos.push(tempdata1);
                    if (i < epiList.length - 1) {
                        extract(i + 1);
                    }
                    if (i == epiList.length - 1) {

                        newSeries.push(data);
                        //sace new anime to database.
                        ani.create(data, function (err, data1) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log('saved')
                            }
                            fs.writeFileSync('animes/new.json', JSON.stringify(newSeries));
                            //twitter tweet

                            twit.updateStatus('check out new anime' + data.title + 'at (www.deathbyanime.com/anime/' + encodeURIComponent(data.title) + ')', function (data2) {
                                console.log('successfully tweeted');
                            });

                            //tweet end
                        });
                    }
                }
            });
        }

        extract(1);
    }
    //add new anime
    addNew = function (data) {
        console.log('add new function enetered');
        url1 = data.pageCatogoryLink;
        request(url1, function (error, response, html) {
            nextPresent = false;
            if (error) {
                console.log('error: ' + error);
                addNew(data);
            }
            if (!error) {
                var $ = cheerio.load(html);
                var tempdata = {};
                tempdata.title = $('.pagetitle').html();
                tempdata.title = tempdata.title.substring(0, tempdata.title.length - 13);
                tempdata.description = $('.catdescription').html();
                if (tempdata.description) {
                    if (tempdata.description.split("/images/").length > 1) {
                        tempdata.image = tempdata.description.split("/images/")[1].split('\">')[0];
                        download('http://gogoanime.com/images/' + tempdata.image, 'public/img/img/' + tempdata.image, function () {
                            console.log('done');
                        });
                    }
                    tempdata.description = tempdata.description.replace(/&amp;/gi, "&");
                    tempdata.description = tempdata.description.replace(/<p>&nbsp;<\/p>/gi, "");
                    tempdata.description = tempdata.description.replace(/<strong>/gi, "");
                    tempdata.description = tempdata.description.replace(/<\/strong>/gi, "");
                    tempdata.description = tempdata.description.replace(/<p>/gi, "");
                    tempdata.description = tempdata.description.replace(/<\/p>/gi, "");
                    tempdata.description = tempdata.description.replace(/<b>/gi, "");
                    tempdata.description = tempdata.description.replace(/<\/b>/gi, "");
                    tempdata.description = tempdata.description.replace(/\n/gi, "");
                    tempdata.description = tempdata.description.replace(/\r/gi, "");
                    tempdata.description = tempdata.description.replace(/&#xA0;/gi, " ");
                    tempdata.description = tempdata.description.replace(/&#x201C;/gi, " ");
                    tempdata.description = tempdata.description.replace(/&#x201D;/gi, " ");
                    tempdata.description = tempdata.description.replace(/&#x2013;/gi, " ");
                    tempdata.description = tempdata.description.replace(/&#x2014;/gi, " ");
                    tempdata.description = tempdata.description.replace(/&quot;/gi, " ");
                    tempdata.description = tempdata.description.replace(/&apos;/gi, "'");
                    if (tempdata.description.split("Genre:").length > 1) {
                        tempdata.genre = [];
                        if (tempdata.description.split("Genre:")[1].split("/").length < tempdata.description.split("Genre:")[1].split(",").length) {
                            tempdata.genre = tempdata.description.split("Genre:")[1].split(",");
                        } else {
                            tempdata.genre = tempdata.description.split("Genre:")[1].split("/");
                        }
                        tempdata.genre[tempdata.genre.length - 1] = tempdata.genre[tempdata.genre.length - 1].substring(0, tempdata.genre[tempdata.genre.length - 1].length - 6);
                    }
                    if (tempdata.description.split("genres:").length > 1) {
                        tempdata.genre = [];
                        if (tempdata.description.split("genres:")[1].split("/").length < tempdata.description.split("genres:")[1].split(",").length) {
                            tempdata.genre = tempdata.description.split("genres:")[1].split(",");
                        } else {
                            tempdata.genre = tempdata.description.split("genres:")[1].split("/");
                        }
                    }
                    if (tempdata.description.split("Plot Summary:").length > 1) {
                        tempdata.description = tempdata.description.split("Plot Summary:")[1].split("Genre")[0];
                    }
                }

                tempdata.videos = [];
                pages(1, url1, tempdata);
            }
        })
    }
}, null, true);